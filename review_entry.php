<?php

// ----INCLUDE APIS------------------------------------
include ("api/api.inc.php");
// ----PAGE GENERATION LOGIC---------------------------
function createFormPage()
{
    $tmethod = appFormMethod();
    $taction = appFormActionSelf();
    $tcontent = <<<PAGE
        <form class="form-horizontal" method="{$tmethod}" action="{$taction}">
        <fieldset>
            <!-- Form Name -->
            <legend>Enter new Review</legend>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="title">Game Title</label>
                    <div class="col-md-4">
                        <select id="title" name="title" class="form-control">
                            <option value="Subnautica">Subnautica</option>
                            <option value="Fifa 20">Fifa 20</option>
                            <option value="Fortnite">Fortnite</option>
                            <option value="Rocket League">Rocket League</option>
                            <option value="Overwatch">Overwatch</option>
                            <option value="Rayman Legends">Rayman Legends</option>
                            <option value="Human: Fall Flat">Human: Fall Flat</option>
                            <option value="Gang Beasts">Gang Beasts</option>
                            <option value="The Crew 2">The Crew 2</option>
                            <option value="Lego Worlds">Lego Worlds</option>
                        </select>
                        <span class="help-block">Choose a game to review</span>
                    </div>
            </div>
            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="note">Review</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="note" name="note"></textarea>
                        <span class="help-block">Enter a review for the game</span>
                    </div>
            </div>
            <!-- Select Score -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="score">Score</label>
                    <div class="col-md-4">
                        <select id="score" name="score" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    <span class="help-block">Select your score of the game</span>
                    </div>
            </div>
            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="form-sub">Submit Form</label>
                    <div class="col-md-4">
                        <button id="form-sub" name="form-sub" type="submit" class="btn btn-danger">Add New Review</button>
                    </div>
            </div>
        </fieldset>
        </form>
        PAGE;
    return $tcontent;
}

// ----BUSINESS LOGIC---------------------------------

$tpagecontent = "";

if(appFormMethodIsPost())
{
    //Get GameId
    $tgameTitle = $_REQUEST["title"] ?? "";
    $tgamesList = DAL_CreateGames("");
    $tgameId = -1;
    foreach($tgamesList->gameitems as $tg){
        if($tg->gameTitle == $tgameTitle) {
            $tgameId = $tg->id;
        }
    }
    //Capture the note Data
    $tnote = processFormData($_REQUEST["note"]  ?? "");
    //Map the Form Data
    $treview = New PLReview();
    $treview->gameId = "{$tgameId}";
    $treview->email = $_SESSION["logged_in"] ?? "";
    $treview->score = $_REQUEST["score"] ?? "";
    $treview->uploadDate = date('d/m/Y');
    $treview->reviewNote = $tnote;

    $tvalid = true;
    if($tvalid)
    {
        $tid = jsonNextReviewID();
        $treview->id = $tid;

        //Convert the Review to json
        $tsavedata = json_encode($treview).PHP_EOL;

        $tfilecontent = file_get_contents("data/json/review.json");
        $tfilecontent .= $tsavedata;
        //Save the file
        file_put_contents("data/json/review.json", $tfilecontent);
        $tpagecontent = "<h1>Review with ID = {$treview->id} has been saved</h1>";
    }
    else
    {
        $tdest = appFormActionSelf();
        $tpagecontent = <<<ERROR
                         <div class="well">
                            <h1>Form was Invalid</h1>
                            <a class="btn btn-warning" href="{$tdest}">Try Again</a>
                         </div>
ERROR;
    }
}
else
{
    //This page will be created by default.
    $tpagecontent = createFormPage();
}
$tpagetitle = "Review Entry Page";
$tpagelead = "";
$tpagefooter = "";

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tpage = new MasterPage($tpagetitle);
// Set the Three Dynamic Areas (1 and 3 have defaults)
if (! empty($tpagelead))
    $tpage->setDynamic1($tpagelead);
$tpage->setDynamic2($tpagecontent);
if (! empty($tpagefooter))
    $tpage->setDynamic3($tpagefooter);
// Return the Dynamic Page to the user.
$tpage->renderPage();
?>