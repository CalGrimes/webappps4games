<?php
// ----INCLUDE APIS------------------------------------
include ("api/api.inc.php");

// ----BUSINESS LOGIC---------------------------------
//Start a sessionfor the user
session_start();

$taction = $_REQUEST["action"] ?? "";
$tlogintoken = $_SESSION["logged_in"] ?? "";
if ($taction == "exit" && ! empty($tlogintoken)) {
    unset($_SESSION["logged_in"]);
    unset($_SESSION["entered"]);
    session_unset();
    session_destroy();
    header("Location: index.php");
} else {
    header("Location: app_error.php");
}

?>