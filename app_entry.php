<?php
// ----INCLUDE APIS------------------------------------
include ("api/api.inc.php");

// ----BUSINESS LOGIC---------------------------------

$tmyemail = $_REQUEST["myemail"] ?? "";
$tmypass = $_REQUEST["mypass"] ?? "";
$tlogintoken = $_SESSION["logged_in"] ?? "";

$tlogindata = jsonLoadAllLogins();
$tloginfound = false;
if (empty($tlogintoken) && !empty($tmyemail) && !empty($tmypass)){
foreach($tlogindata as $tlogin){
    if($tlogin->email == $tmyemail && $tlogin->password == $tmypass){
        $tloginfound = true;
        $_SESSION["logged_in"] = processRequest($tmyemail);
        $_SESSION["entered"] = true;
        header("Location: index.php");
    }
}
if($tloginfound = false) {
    appGoToErrorMsg("Login credentials invalid");
}
}else {
    appGoToError();
}
?>