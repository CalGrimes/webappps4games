<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage()
{
    $tfeaturelist = jsonLoadAllFeatures();
    $tfeaturehtml = "";
    foreach ($tfeaturelist as $tfitem) {
        $tfeaturehtml .= getFeatureHTML($tfitem);
    }
    $tcontent = <<<PAGE
        <div class="consoleOverview">
            <img class="console-image" src="img/PS4.jpg" width="100%" height="100%"></img>
            <p class="text-primary">The PlayStation 4 (officially abbreviated as PS4) is an eighth-generation home video game console developed by Sony Interactive Entertainment</p>
        </div>
        {$tfeaturehtml}  
    PAGE;
    return $tcontent;
}
// ----BUSINESS LOGIC---------------------------------
$tpagecontent = createPage();

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("Console Page");
$tindexpage->setDynamic2($tpagecontent);
$tindexpage->renderPage();
?>