<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");
// ----PAGE GENERATION LOGIC---------------------------
function createPage()
{
    $tgames = DAL_CreateGames("");

    $trecommendations = DAL_CreateRecommendations("sort");

    $tgamehtml = renderGamesTable($tgames, $trecommendations);

    $tcontent = <<<PAGE
            <h2>Game Rankings</h2>
            <div class="row marketing">
    {$tgamehtml}
    </div>
    PAGE;
    return $tcontent;
}
// ----BUSINESS LOGIC---------------------------------
$tpagecontent = createPage();

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("Rankings Page");
$tindexpage->setDynamic2($tpagecontent);
$tindexpage->renderPage();
?>