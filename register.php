<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");
require_once ("api/fn_bll.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage($pmethod, $paction, array $pform)
{
    nullAsEmpty($pform, "inputEmail");
    nullAsEmpty($pform, "inputPassword");
    nullAsEmpty($pform, "confirmPassword");

    nullAsEmpty($pform, "err-email");
    nullAsEmpty($pform, "err-password");
    nullAsEmpty($pform, "err-confirmpassword");
    nullAsEmpty($pform, "err-passwordmatch");

    $tcontent = <<<PAGE
        <div class="jumbotron">
            <h2>Register</h2>
        </div>
        <form class="form-horizontal" method="{$pmethod}" action="{$paction}">
           <div class="form-group">
                <label class="control-label col-xs-3" for="email">Email:</label>
                    <div class="col-xs-9">
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter Email" value="{$pform["inputEmail"]}">
                        {$pform["err-email"]}
                    </div>
            </div>
           <div class="form-group">
                <label class="control-label col-xs-3" for="password">Password:</label>
                    <div class="col-xs-9">
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Enter Password" value="{$pform["inputPassword"]}"> 
                        {$pform["err-password"]}
                    </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3" for="confirmPassword">Confirm Password:</label>
                    <div class="col-xs-9">
                        <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password" value="{$pform["confirmPassword"]}">
                         {$pform["err-passwordmatch"]}
                         {$pform["err-confirmpassword"]} 
                       
                    </div>
            </div>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <div class="checkbox-inline">
                        <label><input type="checkbox" id="checkNews" name="checkNews" value="news" checked> Send me latest news and updates.</label>
                    </div>
                </div>
            </div>
             <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <div class="checkbox-inline">
                        <label><input type="checkbox" id="checkTerms" name="checkTerms" value="agree" checked>I agree to the <a href="#"> Terms and Conditions</a></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <div class="checkbox">
                        <label><input type="checkbox" id="rememberMe" name="rememberMe"> Remember me</label>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="submit" class="btn btn-primary" value="Register">
                    <input type="submit" class="btn btn-default" value="Reset">
                </div>
            </div>
        </form>
    PAGE;
    return $tcontent;
}

function createResponse(array $pformdata)
{
    if ($pformdata["checkNews"] = checked) {
        $tresponse = <<<RESPONSE
                <section class="panel panel-primary" id="Form Response">
                    <div class="jumbotron>"
                        <h1>Thank You!</h1>
                        <p class="lead">Your account has been created. Thank you for keeping up to date with PS4 games!</p>
                        <p class="lead">You will recieve weekly updates to {$pformdata["inputEmail"]}</p>
                    </div>
                </section>
        RESPONSE;
    } else {
        $tresponse = <<<RESPONSE
                <section class="panel panel-primary" id="Form Response"
                    <div class="jumbotron>"
                        <h1>Thank You!</h1>
                        <p class="lead">Your account has been created. Thank you for keeping up to date with PS4 games!</p>
                    </div>
                </section>
        RESPONSE;
    }
    return $tresponse;
}

function processForm(array $pformdata): array
{
    foreach ($pformdata as $tfield => $tvalue) {
        $pformdata[$tfield] = processFormData($tvalue);
    }

    $tvalid = true;
    if ($tvalid && empty($pformdata["inputEmail"])) {
        $tvalid = false;
        $pformdata["err-email"] = "<p id=\"help-email\" class=\"help-block\">Email Required</p>";
    }
    if ($tvalid && empty($pformdata["inputPassword"])) {
        $tvalid = false;
        $pformdata["err-password"] = "<p id=\"help-password\" class=\"help-block\">Password Required</p>";
    }
    if ($tvalid && empty($pformdata["confirmPassword"])) {
        $tvalid = false;
        $pformdata["err-confirmpassword"] = "<p id=\"help-confirmpassword\" class=\"help-block\">Password Required</p>";
    }
    if ($tvalid && $pformdata["confirmPassword"] != $pformdata["inputPassword"]) {
        $tvalid = false;
        $pformdata["err-passwordmatch"] = "<p id=\"help-passwordmatch\" class=\"help-block\">Password must match</p>";
    }
    if ($tvalid) {
        $pformdata["valid"] = true;
    }

    return $pformdata;
}
// ----BUSINESS LOGIC---------------------------------
$taction = htmlspecialchars($_SERVER['PHP_SELF']);
$tmethod = "GET";

$tformdata = processForm($_REQUEST) ?? array();


$tregister = New PLLogin();
$tregister->email = $_REQUEST["inputEmail"] ?? "";
$tregister->password = $_REQUEST["inputPassword"] ?? "";

if (isset($tformdata["valid"])) {
    $tid = jsonNextLoginID();
    $tregister->id = $tid;

    //Convert the Login to json
    $tsavedata = json_encode($tregister).PHP_EOL;

    $tfilecontent = file_get_contents("data/json/login.json");
    $tfilecontent .= $tsavedata;

    //Save the file
    file_put_contents("data/json/login.json", $tfilecontent);

    $tpagecontent = createResponse($tformdata);
} else {
    $tpagecontent = createPage($tmethod, $taction, $tformdata);
}

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tindexpage = new MasterPage("Register");
$tindexpage->setDynamic2($tpagecontent);
$tindexpage->renderPage();
?>