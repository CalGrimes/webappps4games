<?php
// ----INCLUDE APIS------------------------------------
// Include our Website API
include ("api/api.inc.php");

// ----PAGE GENERATION LOGIC---------------------------
function createPage()
{
    $tgameslist = DAL_CreateGames("shuffle");
    $tgameshtml = "";
    $counter = 0;

    foreach ($tgameslist->gameitems as $tgitem) {
        if($counter < 3){
            $tgameshtml .= $tgitem->getSummaryHtml();
        }
        $counter += 1;
    }



    $tcontent = <<<PAGE
        <div class="jumbotron">
            <h2>Summary of Games</h2>
        </div>
        <div class="row">
            {$tgameshtml}
        </dv>
    PAGE;
    return $tcontent;
}
// ----BUSINESS LOGIC---------------------------------
$tpagetitle = "Home Page";
$tpagelead = "";
$tpagecontent = createPage();
$tpagefooter = "";

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tpage = new MasterPage($tpagetitle);
// Set the Three Dynamic Areas (1 and 3 have defaults)
if (! empty($tpagelead))
    $tpage->setDynamic1($tpagelead);
$tpage->setDynamic2($tpagecontent);
if (! empty($tpagefooter))
    $tpage->setDynamic3($tpagefooter);
// Return the Dynamic Page to the user.
$tpage->renderPage();
?>