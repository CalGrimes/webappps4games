<?php

class BLLGameItem
{

    public $id = null;

    public $gameTitle;

    public $description;

    public $score;

    public $image;

    public $source;

    // -------CONSTRUCTORS------------------
    public function __construct($pid = -1, $ptitle = "Default Title", $pdescription = "Default Title", $pscore = -1, $pimg = "defualt.jpg", $psource = "#")
    {
        $this->id = $pid;
        $this->gameTitle = $ptitle;
        $this->description = $pdescription;
        $this->score = $pscore;
        $this->image = $pimg;
        $this->source = $psource;
    }

    // -------METHODS-----------------------
    public function getSummaryHTML()
    {

        $tgameitem = <<<RI
            <div class="column">
                <h2 class="game-title">{$this->gameTitle}</h2>
                <p>
                    <img class="game-image" src="img/{$this->image}" width="240" height="240"></img>
                </p>
                <p class="text-primary">
                <br /> Summary: {$this->gameTitle} {$this->description}
                <br /> <a href="game.php?id={$this->id}">More...</a>
                </p>
                <p>
                    <h4 class="score">
                        Score <a class="score-btn"
                        href="rankings.php">{$this->score}</a>
                    </h4>
            </div>
        RI;
        return $tgameitem;
    }
    public function getConsoleHTML()
    {

        $tconsoleItem = <<<CI
             <article class="game-center">
                    <h2 class="text-center">Game Details</h2>
                    <div class="well">
                        <h1>{$this->gameTitle}</h1>
                    </div>
                    <p><img src="img/{$this->image}" width="240" height="240"></img>
                    <p class="text-primary">
                        <br /><input type="button" onclick="location.href ='console.php';" value="Playstation 4">
                        <br /> {$this->gameTitle} {$this->description}
                         <input type="button" onclick="window.location.href = '{$this->source}';" value="More...">
                    </p>
                    <h5 class="score">
                        Score: {$this->score}
                    </h5>
                </article>
        CI;
        return $tconsoleItem;
    }
}
class BLLGamesList
{

    public $gameitems = array();

    public function __construct()
    {
        $this->gameitems = [];
    }
}
?>