<?php
require_once ("oo_bll.inc.php");

function renderRankRow($trank)
{
    $trow = <<<RANK
        		<tr>
        		    <td>{$trank}</td>
    RANK;
    return $trow;
}
function renderGameRow(BLLGameItem $pg)
{
    $tqs = "game.php?id={$pg->id}";
    $tlink = "<a href=\"{$tqs}\">View...</a>";
    $trow = <<<PROW
        		    <td>{$pg->gameTitle}</td>
                    <td>{$pg->score}</td>
                    <td>{$tlink}</td>
    PROW;
    return $trow;
}
function renderRatingRow(PLRecommendationItem $pr)
{
    $trow = <<<RROW
                    <td>{$pr->rating}</td>
    RROW;
    return $trow;
}
function renderReviewRow(BLLGameItem $pg)
{
    $treviews = jsonLoadAllReviews();

    $treviewScore = 0;
    $counter = 0;
    foreach($treviews as $tr){
        if($tr->gameId == $pg->id){
            $treviewScore += $tr->score;
            $counter += 1;
        }
        if ($counter != 0){
            $treviewScore /= $counter;
        } else{
            $treviewScore = "N/A";
        }
    }
    $trow = <<<RROW
                    <td>{$treviewScore}</td>
                    </tr>
    RROW;
    return $trow;
}

function renderGamesTable(BLLGamesList $pgamesList, PLRecommendationList $precomlist)
{
    $trowdata = "";
    $trank = 0;

    foreach($precomlist->recommendationitems as $tr) {
        $trank += 1;
        $trowdata .= renderRankRow($trank);
        foreach($pgamesList->gameitems as $tg){
            if ($tg->id == $tr->gameId){
                $trowdata .= renderGameRow($tg);
                $trowdata .= renderRatingRow($tr);
                $trowdata .= renderReviewRow($tg);
            }

        }

    }

    $ttable = <<<TABLE
    <table class="table table-striped table-hover">
    					<thead>
    						<tr>
    							<th>Rank</th>
    							<th>Title</th>
                                <th>Game Score</th>
                                <th>Link</th>
                                <th>Recommendation Rating</th>
                                <th>Review Score</th>
    						</tr>
    					</thead>
    					<tbody>
    					{$trowdata}
    					</tbody>
    </table>
    TABLE;
    return $ttable;
}
function getReviewHTML(PLReview $pr)
{
    $treviewItem = <<<RI
                <p><b>Posted by:</b> {$pr->email}</h5>
                <div class="text-primary">
                    <p><b>Review:</b> {$pr->reviewNote}
                    <p><b>Score:</b> {$pr->score}
                    <p><b>Posted Date:</b> {$pr->uploadDate}
                </div>
                <hr>
        RI;
    return $treviewItem;
}
function getFeatureHTML(PLFeature $pf)
{
    $tfeatureItem = <<<CF
                    <div class="consoleFeatures">
                        <h4>{$pf->featureName}</h4>
                        <img class="feature-image" src="img/{$pf->featureImage}" width="100%" length="100%"</img>
                        <p class="text-primary">{$pf->description}</p>
                    </div>
        CF;
    return $tfeatureItem;
}
function getOffReviewHTML(PLOfficialReview $offr)
{
    $toffReview = <<<RI
            <article class="col-xs-6">
                <h4>Official Product Reviews:</h4>
                <div class="text-primary">
                    <p><input type="button" onclick="windows.location.href = '{$offr->review1}';" value="{$offr->review1}">
                    <p><input type="button" onclick="windows.location.href = '{$offr->review2}';" value="{$offr->review2}">
                    <p><input type="button" onclick="windows.location.href = '{$offr->review3}';" value="{$offr->review3}">
                </div>
            </article>
        RI;
    return $toffReview;
}
function getConsumerInfoHTML(PLConsumerInfo $ci)
{
    $tconsumerInfo = <<<CI
                <h5>{$ci->retailer}</h5>
                <div class="text-primary">
                    <p>Price: {$ci->price}
                    <p>Information: {$ci->information}
                    <p>Site: <a href="{$ci->link}">Link</a>
                </div>
        CI;
    return $tconsumerInfo;
}
function getKeyFactsHTML($kf)
{
    $tkeyFactItem = <<<KF
            <article class="col-xs-6">
                <h4>Key Facts:</h4>
                <div class="text-primary">
                    <p>Created by: {$kf->createdBy}
                    <p>Release date: {$kf->releaseDate}
                    <p>Platforms: {$kf->platforms}
                    <p>Genre: {$kf->genre}
                    <p>Age Rating: {$kf->ageRating}
                </div>
            </article>
        KF;
    return $tkeyFactItem;
}
?>