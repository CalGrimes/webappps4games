<?php
class PLFeature
{

    // -------CLASS FIELDS------------------
    public $id = null;

    public $featureName;

    public $description;

    public $featureImage;

    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $tkey => $tvalue)
        {
            $this->{$tkey} = $tvalue;
        }
    }
}


class PLOfficialReview
{

    // -------CLASS FIELDS------------------
    public $id = null;

    public $gameId = null;

    public $review1;

    public $review2;

    public $review3;

    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $tkey => $tvalue)
        {
            $this->{$tkey} = $tvalue;
        }
    }


}


class PLUserReviewItem
{

    // -------CLASS FIELDS------------------
    public $gameId;

    public $user;

    public $reviewNote;

    public $rating;

    public $link;

    // -------CONSTRUCTORS------------------
    public function __construct($pgid = "-1", $puser = "Anonymous", $prnote = "Default Review", $prate = "-1", $link = "app_error.php")
    {
        $this->gameId = $pgid;
        $this->user = $puser;
        $this->reviewNote = $prnote;
        $this->rating = $prate;
    }

    // -------METHODS-----------------------
    public function getReviewHTML()
    {
        $tuserReviewItem = <<<RI
                    <div class="gameFeedback">
                        <h4>Posted by: {$this->user}</h4>
                        <p class="text-primary">Review: {$this->reviewNote}</p>
                        <h5>Rating: {$rating}</h5>
                    </div>
        RI;
        return $tuserReviewItem;
    }
}

class PLUserReviewList
{

    public $reviewitems = array();

    public function __construct()
    {
        $this->reviewitems = [];
    }
}

class PLRecommendationItem
{

    // -------CLASS FIELDS------------------
    public $gameId;

    public $recommndationNote;

    public $rating;

    // -------CONSTRUCTORS------------------
    public function __construct($pgid = "-1", $prnote = "Default Recommendation", $prate = "-1")
    {
        $this->gameId = $pgid;
        $this->recommendationNote = $prnote;
        $this->rating = $prate;
    }

    // -------METHODS-----------------------
    public function getRecommendationHTML()
    {
        $trecommendationItem = <<<RI
            <article class="col-xs-6">
                <h4>Recommendation:</h4>
                <div class="text-primary">
                    <p>{$this->recommendationNote}
                    <h5 class="text-primary">
                        Rating: {$this->rating}
                    </h5>
                </div>
            </article>
        RI;
        return $trecommendationItem;
    }
}

class PLRecommendationList
{

    public $recommendationitems = array();

    public function __construct()
    {
        $this->recommendationitems = [];
    }
}

class PLKeyFacts
{
    public $id = null;

    public $gameId;

    public $createdBy;

    public $releaseDate;

    public $platforms;

    public $genre;

    public $ageRating;

    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $tkey => $tvalue)
        {
            $this->{$tkey} = $tvalue;
        }
    }
}

class PLConsumerInfoItem
{

    public $gameId;

    public $retailer;

    public $price;

    public $information;

    public $link;


    public function __construct($pgid = "-1", $pretail = "Default Retailer", $pprice = "�0.00", $pinfo = "Default Information", $plink = "app_err.php")
    {
        $this->gameId = $pgid;
        $this->retailer = $pretail;
        $this->price = $pprice;
        $this->information = $pinfo;
        $this->link = $plink;
    }
    // -------METHODS-----------------------
    public function getConsumerInfoHTML()
    {
        $tconsumerInfoItem = <<<CI
                <h5>{$this->retailer}</h5>
                <div class="text-primary">
                    <p>Price: {$this->price}
                    <p>Information: {$this->information}
                    <p>Site: <a href="{$this->link}">Link</a>
                </div>
        CI;
        return $tconsumerInfoItem;
    }
}

class PLConsumerInfoList
{

    public $consumerItems = array();

    public function __construct()
    {
        $this->consumeritems = [];
    }

}

class PLReview
{
    public $id = null;

    public $gameId;

    public $email;

    public $reviewNote;

    public $score;

    public $uploadDate;

    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $tkey => $tvalue)
        {
            $this->{$tkey} = $tvalue;
        }
    }
}
class PLLogin
{

    // -------CLASS FIELDS------------------
    public $id = null;

    public $email;

    public $password;

    public $favgame;

    public function fromArray(stdClass $passoc)
    {
        foreach($passoc as $tkey => $tvalue)
        {
            $this->{$tkey} = $tvalue;
        }
    }
}
?>