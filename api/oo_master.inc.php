<?php
// Include our HTML Page Class
require_once ("oo_page.inc.php");
// Start up a PHP Session for the user.
session_start();
class MasterPage
{

    // -------FIELD MEMBERS----------------------------------------
    private $_htmlpage;

    // Holds our Custom Instance of an HTML Page
    private $_dynamic_1;

    // Field Representing our Dynamic Content #1
    private $_dynamic_2;

    // Field Representing our Dynamic Content #2
    private $_game_ids;

    // -------CONSTRUCTORS-----------------------------------------
    function __construct($ptitle)
    {
        $this->_htmlpage = new HTMLPage($ptitle);
        $this->setPageDefaults();
        $this->setDynamicDefaults();
    }

    // -------GETTER/SETTER FUNCTIONS------------------------------
    public function getDynamic1()
    {
        return $this->_dynamic_1;
    }

    public function getDynamic2()
    {
        return $this->_dynamic_2;
    }

    public function setDynamic1($phtml)
    {
        $this->_dynamic_1 = $phtml;
    }

    public function setDynamic2($phtml)
    {
        $this->_dynamic_2 = $phtml;
    }

    // -------PUBLIC FUNCTIONS-------------------------------------
    public function createPage()
    {
        // Create our Dynamic Injected Master Page
        $this->setMasterContent();
        // Return the HTML Page..
        return $this->_htmlpage->createPage();
    }

    public function renderPage()
    {
        // Create our Dynamic Injected Master Page
        $this->setMasterContent();
        // Echo the page immediately.
        $this->_htmlpage->renderPage();
    }

    public function addCSSFile($pcssfile)
    {
        $this->_htmlpage->addCSSFile($pcssfile);
    }

    public function addScriptFile($pjsfile)
    {
        $this->_htmlpage->addScriptFile($pjsfile);
    }

    // -------PRIVATE FUNCTIONS-----------------------------------
    private function setPageDefaults()
    {
        $this->_htmlpage->setMediaDirectory("css", "js", "fonts", "img", "data");
        $this->addCSSFile("bootstrap.css");
        $this->addCSSFile("site.css");
        $this->addScriptFile("jquery-2.2.4.js");
        $this->addScriptFile("bootstrap.js");
        $this->addScriptFile("holder.js");
        $this->_game_ids = [1,3,4,5,6,8,11,12,13,15];
    }

    private function setDynamicDefaults()
    {
        $this->_dynamic_1 = <<<JUMBO
            <h1>Playstation 4</h1>
            <p class="lead">Greatness Awaits.</p>
        JUMBO;
        $this->_dynamic_2 = "";
    }

    private function setMasterContent()
    {
        $tlogin = "app_entry.php";
        $tlogout = "app_exit.php";
        $tentryhtml = <<<FORM
            <form id="login" action="{$tlogin}" method="post"
            class="navbar-form navbar-right" role="form">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-user"></i>        
                    </span> 
                    <input id="email" type="email" class="form-control"
                        name="myemail" value="" placeholder="Email">
                </div>
                <div class="input-group">
                    <input id="password" type="password" class="form-control"
                        name="mypass" value="" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
            </form>  
        FORM;
        $tlogintoken = $_SESSION["logged_in"] ?? "";
        $texithtml = <<<EXIT
            <div class="navbar-right">
                {$tlogintoken}
                <a class="btn btn-info" href="{$tlogout}?action=exit">Logout</a>
            </div>
        EXIT;

        $tauth = isset($_SESSION["logged_in"]) ? $texithtml : $tentryhtml;
        $tcurryear = date("Y");
        $tid = $this->_game_ids[array_rand($this->_game_ids, 1)];

        $tmasterpage = <<<MASTER
        <div class="container">
        	<div class="header clearfix">
        		<nav>
                    {$tauth}
                    <ul class="nav nav-pills pull-left">         
                        <li role="presentation"><a href="index.php">Home</a></li>
                        <li role="presentation"><a href="console.php">Console</a></li>
                        <li role="presentation"><a href="rankings.php">Rankings</a></li>
                    </ul>
        		</nav>
        	</div>
        	<div class="jumbotron">
                {$this->_dynamic_1}
            </div>
            <div class="row marketing">
                {$this->_dynamic_2}
            </div>
            <footer class="footer">
                <div class="column">
                    <p><b>About this console</b>
                    <p><a href="https://www.playstation.com/en-gb/explore/ps4/">Console Overview</a>
        	    </div>
                <div class="column">
                    <p><b>Related Content</b>
                    <p><a href="https://www.playstation.com/en-gb/explore/ps4/accessories/">Accessories</a>
                    <p><a href="https://www.playstation.com/en-gb/explore/ps4/games/upcoming-ps4-games/">Upcoming Games</a>
                    <p><a href="https://en.wikipedia.org/wiki/PlayStation_4">Wikipedia</a>
                </div>
                <div class="column">
                    <p>Cal Grimes - LJMU Student &copy {$tcurryear}</p>
                    <p><a href="register.php">Create an account</a>
                    <p><a href="game.php?id={$tid}">Lucky Dip</a>
            </footer>
        </div>
        MASTER;
        $this->_htmlpage->setBodyContent($tmasterpage);
    }
}

?>