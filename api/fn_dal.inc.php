<?php

// Include the Class Include
require_once ("oo_bll.inc.php");
require_once ("oo_pl.inc.php");

function DAL_CreateGames($shuffle): BLLGamesList
{
    $tgamedata = [];
    $tgamedata["1"] = new BLLGameItem(1, "Subnautica", "is an open-world survival action-adventure video game developed and published by Unknown Worlds Entertainment. It allows the player to freely explore the ocean on an alien planet, known as planet 4546B, after their spaceship crashes on the planet's surface.", "80", "subnautica.png", "https://www.metacritic.com/game/playstation-4/subnautica");
    $tgamedata["3"] = new BLLGameItem(3, "Fifa 20", "is a football simulation video game published by Electronic Arts as part of the FIFA series.", "78", "fifa20.jpg", "https://www.metacritic.com/game/playstation-4/fifa-20");
    $tgamedata["4"] = new BLLGameItem(4, "Fortnite", "is a survival game where 100 players fight against each other in player versus player combat to be the last one standing.", "78", "fortnite.jpg", "https://www.metacritic.com/game/playstation-4/fortnite");
    $tgamedata["5"] = new BLLGameItem(5, "Rocket League", "is a vehicular soccer video game developed and published by Psyonix.", "85", "rocketleague.png", "https://www.metacritic.com/game/playstation-4/rocket-league");
    $tgamedata["6"] = new BLLGameItem(6, "Overwatch", "is a team-based multiplayer first-person shooter developed and published by Blizzard Entertainment", "90", "overwatch.jpg", "https://www.metacritic.com/game/playstation-4/overwatch");
    $tgamedata["8"] = new BLLGameItem(8, "Rayman Legends", "is a game where up to four players simultaneously make their way through various levels. Lums can be collected by touching them, defeating enemies, or freeing captured Teensies. Collecting Teensies unlocks new worlds, which can be played in any order once they are available.", "90", "raymanlegends.jpg", "https://www.metacritic.com/game/playstation-4/rayman-legends");
    $tgamedata["11"] = new BLLGameItem(11, "Human: Fall Flat", "is a quirky, open-ended physics-based puzzle and exploration game set in floating dreamscapes. Your goal is to escape these surreal dreams by solving puzzles with nothing but your wits and physics.", "67", "fallflat.jpg", "https://www.metacritic.com/game/playstation-4/human-fall-flat");
    $tgamedata["12"] = new BLLGameItem(12, "Gang Beasts", "is a silly local multiplayer party game with doughy ragdoll physics and horrific environmental hazards. In the current pre-alpha players can grab, push, pull, punch, and throw their friends from wrestling rings, speeding trucks, and suspended platforms.","68", "gangbeasts.jpg", "https://www.metacritic.com/game/playstation-4/gang-beasts");
    $tgamedata["13"] = new BLLGameItem(13, "The Crew 2", "is the newest iteration in the franchise, The Crew 2 captures the thrill of the American motorsports spirit in an open world. Welcome to Motornation, a huge, varied, action-packed playground built for motorsports throughout the entire US of A.", "64", "crew2.jpg", "https://www.metacritic.com/game/playstation-4/the-crew-2");
    $tgamedata["15"] = new BLLGameItem(15, "Lego Worlds", "is an open environment of procedurally-generated Worlds made entirely of LEGO bricks which you can freely manipulate and dynamically populate with LEGO models.", "66", "legoworlds.jfif", "https://www.metacritic.com/game/playstation-4/lego-worlds");

    if($shuffle == "shuffle"){
        shuffle($tgamedata);
    }

    $tgamelist = new BLLGamesList();
    $tgamelist->gameitems = $tgamedata;
    return $tgamelist;
}
function DAL_CreateRecommendations($sort): PLRecommendationList
{
    $trecommendationData = [];
    $trecommendationData["1"] = new PLRecommendationItem(1, "Great game, recommend to anyone who likes open-world survival and lots of water", "7");
    $trecommendationData["3"] = new PLRecommendationItem(3, "Good game, recommend to anyone who loves football and fast-paced action", "6");
    $trecommendationData["4"] = new PLRecommendationItem(4, "Great game, recommend to anyone who enjoys battle royale themed action and continuous new experiences every week", "8");
    $trecommendationData["5"] = new PLRecommendationItem(5, "Good game, recommend to anyone who likes cars and football blended into one", "5");
    $trecommendationData["6"] = new PLRecommendationItem(6, "Great game, recommend to anyone who loves fast-paced action and challenging competition", "8");
    $trecommendationData["8"] = new PLRecommendationItem(8, "Okay game, recommend to people who enjoy a progressively more difficult game that involoves lots of unique levels to complete", "4");
    $trecommendationData["11"] = new PLRecommendationItem(11, "Good game, recommend to people who enjoy a fun and interactive game to play with friends", "7");
    $trecommendationData["12"] = new PLRecommendationItem(12, "Okay game, recommend to people who like to enjoy battling against their friends", "5");
    $trecommendationData["13"] = new PLRecommendationItem(13, "Good game, recommend to people who enjoy open-world racing games", "8");
    $trecommendationData["15"] = new PLRecommendationItem(15, "Good game, recommend to people who enjoy sandbox games this time it's lego themed", "6");

    if($sort == "sort") {
        function comparator($object1, $object2)
        {
            return $object1->rating < $object2->rating;
        }

        // Use usort and the comparator function to sort the array
        usort($trecommendationData, 'comparator');
    }

    $trecommendations = new PLRecommendationList();
    $trecommendations->recommendationitems= $trecommendationData;
    return $trecommendations;


}
function DAL_CreateConsumerInfo(): PLConsumerInfoList
{
    $tconsumerInfoData = [];
    $tconsumerInfoData["1"] = new PLConsumerInfoItem(1, "GAME", "�19.99", "Game is the UK's leading games retailer with great deals on video games, consoles, accessories and the latest pre-order games.", "https://www.game.co.uk/en/subnautica-2446432?gclid=Cj0KCQjwncT1BRDhARIsAOQF9Lk1hCz5WNc3_yj3-tBPLHPLt4vm7Mn4TXcVP0_mma6rBZB9ApAnsmMaAnSjEALw_wcB");
    $tconsumerInfoData["2"] = new PLConsumerInfoItem(1, "Argos", "�18.99", "Argos is an English catalogue retailer operating in the United Kingdom and Ireland, and a subsidiary of Sainsbury's.", "https://www.argos.co.uk/product/9160886?istCompanyId=a74d8886-5df9-4baa-b776-166b3bf9111c&istFeedId=c290d9a9-b5d6-423c-841d-2a559621874c&istItemId=ixwmwqtmw&istBid=t&&cmpid=GS001&_\$ja=tsid:59157%7cacid:534-693-8244%7ccid:9548734227%7cagid:97250270545%7ctid:pla-884259347416%7ccrid:422732075249%7cnw:g%7crnd:1254440374368560269%7cdvc:c%7cadp:%7cmt:%7cloc:1007213&utm_source=Google&utm_medium=cpc&utm_campaign=9548734227&utm_term=&utm_content=shopping&utm_custom1=97250270545&utm_custom2=534-693-8244&gclid=Cj0KCQjwncT1BRDhARIsAOQF9LlULw2mJ4ew-SReQhvy4CpRC9hhrmGQJG8XSGG8tNj2faax2HE2VsIaAv-GEALw_wcB&gclsrc=aw.ds");
    $tconsumerInfoData["3"] = new PLConsumerInfoItem(1, "Base", "�19.99", "Video Games, PC, Music CDs, Blu-ray, DVD, Storage, Flash Memory and more. All at competitive prices. Free UK Delivery on all purchases.", "https://www.base.com/Games/Subnautica-PS4/SUBPS4/product.htm?awc=2694_1588688907_c89c70a3ee3df0ed3d6357f958bf3dba");
    $tconsumerInfoData["7"] = new PLConsumerInfoItem(3, "GAME", "�32.99", "Game is the UK's leading games retailer with great deals on video games, consoles, accessories and the latest pre-order games.", "https://www.game.co.uk/en/fifa-20-2622573?gclid=Cj0KCQjwncT1BRDhARIsAOQF9LmlJDGsKt18FEGtW6kCDa1lKNrv7mfJCjakTlreE_hFkZ-7EB9W7rQaAvvfEALw_wcB");
    $tconsumerInfoData["8"] = new PLConsumerInfoItem(3, "Argos", "�30.99", "Argos is an English catalogue retailer operating in the United Kingdom and Ireland, and a subsidiary of Sainsbury's.", "https://www.argos.co.uk/product/3246500?istCompanyId=a74d8886-5df9-4baa-b776-166b3bf9111c&istFeedId=c290d9a9-b5d6-423c-841d-2a559621874c&istItemId=iwlraiplm&istBid=t&&cmpid=GS001&_\$ja=tsid:59157%7cacid:534-693-8244%7ccid:9548734227%7cagid:97250270545%7ctid:pla-884259326296%7ccrid:422732075249%7cnw:g%7crnd:16357859584541811045%7cdvc:c%7cadp:%7cmt:%7cloc:1007213&utm_source=Google&utm_medium=cpc&utm_campaign=9548734227&utm_term=&utm_content=shopping&utm_custom1=97250270545&utm_custom2=534-693-8244&gclid=Cj0KCQjwncT1BRDhARIsAOQF9Ln1-NEOMoqalCvC0t6LOTjN-MTrB5hABDeozno0mOMQGEwmAg5vRssaAjuqEALw_wcB&gclsrc=aw.ds");
    $tconsumerInfoData["9"] = new PLConsumerInfoItem(3, "Currys", "�29.99", "Massive Savings on TVs, Washing Machines, Cookers, Laptops, Headphones, Cameras, Tablets & more. We're Open Online and Delivering!", "https://www.currys.co.uk/gbuk/tv-and-home-entertainment/gaming/console-games/ps4-fifa-20-10194309-pdt.html?istCompanyId=bec25c7e-cbcd-460d-81d5-a25372d2e3d7&istFeedId=2f489429-2329-4fcd-941a-a6a497a10e1d&istItemId=waatxmllq&istBid=t&awc=1599_1588688235_7e75a49ce79b2ef2b2336dfad8fa25a4&srcid=369&xtor=AL-1&cmpid=aff~Kelkoo~Comparison%20Engine~74988~Kelkoo+Group");
    $tconsumerInfoData["10"] = new PLConsumerInfoItem(4, "Playstation store", "FREE", "The PlayStation Store is a digital media store available to users of Sony's PlayStation 3, PlayStation 4, PlayStation Vita game consoles via the PlayStation Network.", "https://store.playstation.com/en-gb/product/EP1464-CUSA07669_00-FORTNITETESTING1");
    $tconsumerInfoData["11"] = new PLConsumerInfoItem(5, "GAME", "�19.99", "Game is the UK's leading games retailer with great deals on video games, consoles, accessories and the latest pre-order games.", "https://www.game.co.uk/en/rocket-league-collectors-edition-2186946?gclid=Cj0KCQjwncT1BRDhARIsAOQF9LnD06SueUZBgSmsTFYJIMdSAX7OPKM--UEKyhb2IpXhmlg15XcDoHQaAnMpEALw_wcB");
    $tconsumerInfoData["12"] = new PLConsumerInfoItem(5, "Argos", "�16.49", "Argos is an English catalogue retailer operating in the United Kingdom and Ireland, and a subsidiary of Sainsbury's.", "https://www.argos.co.uk/product/8246701?istCompanyId=a74d8886-5df9-4baa-b776-166b3bf9111c&istFeedId=c290d9a9-b5d6-423c-841d-2a559621874c&istItemId=ixwmwqtml&istBid=t&&cmpid=GS001&_\$ja=tsid:59157%7cacid:534-693-8244%7ccid:9548734227%7cagid:97250270545%7ctid:pla-884259340976%7ccrid:422732075249%7cnw:g%7crnd:16324930376886074147%7cdvc:c%7cadp:%7cmt:%7cloc:1007213&utm_source=Google&utm_medium=cpc&utm_campaign=9548734227&utm_term=&utm_content=shopping&utm_custom1=97250270545&utm_custom2=534-693-8244&gclid=Cj0KCQjwncT1BRDhARIsAOQF9LmcsuKjZ2Vs8MKW0t076ignMUvVsKVJUcfB_Sat7RLhqEIXcYapCPgaAkTiEALw_wcB&gclsrc=aw.ds");
    $tconsumerInfoData["13"] = new PLConsumerInfoItem(5, "The Game Collection", "�12.95", "Buy cheap video games and accessories for PlayStation 4, Xbox One, Nintendo Switch, PC gaming, 3DS, PS3, Xbox 360.", "https://www.thegamecollection.net/rocket-league-ultimate-edition-ps4/?utm_source=tradedoubler&utm_medium=affiliate&utm_campaign=td&tduid=7f6b2a91df26a20bb94c934d39fc211b");
    $tconsumerInfoData["14"] = new PLConsumerInfoItem(6, "GAME", "�29.99", "Game is the UK's leading games retailer with great deals on video games, consoles, accessories and the latest pre-order games.", "https://www.game.co.uk/en/overwatch-game-of-the-year-edition-2018417?gclid=Cj0KCQjwncT1BRDhARIsAOQF9LmhmzNfOrto_YfNnAoR-nLieWH230KrN51bVvwpHggyxP8l9kaE-fAaAsn-EALw_wcB");
    $tconsumerInfoData["15"] = new PLConsumerInfoItem(6, "Argos", "�29.99", "Argos is an English catalogue retailer operating in the United Kingdom and Ireland, and a subsidiary of Sainsbury's.", "https://www.argos.co.uk/product/8359344?istCompanyId=a74d8886-5df9-4baa-b776-166b3bf9111c&istFeedId=c290d9a9-b5d6-423c-841d-2a559621874c&istItemId=ixwmwqxwi&istBid=t&&cmpid=GS001&_\$ja=tsid:59157%7cacid:534-693-8244%7ccid:9548734227%7cagid:97250270545%7ctid:pla-884259341736%7ccrid:422732075249%7cnw:g%7crnd:11125971529564891136%7cdvc:c%7cadp:%7cmt:%7cloc:1007213&utm_source=Google&utm_medium=cpc&utm_campaign=9548734227&utm_term=&utm_content=shopping&utm_custom1=97250270545&utm_custom2=534-693-8244&gclid=Cj0KCQjwncT1BRDhARIsAOQF9LlHFPgWfEVwjDYxWKMZ7zwiEcOg7DVfpakPc04ADcExjJUJ3VtxsgcaAnN6EALw_wcB&gclsrc=aw.ds");
    $tconsumerInfoData["16"] = new PLConsumerInfoItem(6, "Galactic Games", "�28.85", "Game store selling cheap games. We sell a wide variety of new and pre-owned games which include PS4, Xbox One, Wii U, Nintendo Switch games.", "https://galacticgames.co.uk/overwatch-origins-edition-for-ps4.html?language=en&currency=GBP");
    $tconsumerInfoData["20"] = new PLConsumerInfoItem(8, "Argos", "�12.99", "Argos is an English catalogue retailer operating in the United Kingdom and Ireland, and a subsidiary of Sainsbury's.", "https://www.argos.co.uk/product/4163864?istCompanyId=a74d8886-5df9-4baa-b776-166b3bf9111c&istFeedId=c290d9a9-b5d6-423c-841d-2a559621874c&istItemId=ipxaawmrt&istBid=t&&cmpid=GS001&_\$ja=tsid:59157%7cacid:534-693-8244%7ccid:9548734227%7cagid:97250270545%7ctid:pla-884259328736%7ccrid:422732075249%7cnw:g%7crnd:15742667348694149479%7cdvc:c%7cadp:%7cmt:%7cloc:1007213&utm_source=Google&utm_medium=cpc&utm_campaign=9548734227&utm_term=&utm_content=shopping&utm_custom1=97250270545&utm_custom2=534-693-8244&gclid=Cj0KCQjwncT1BRDhARIsAOQF9LkU5cn7VfAvMi5KW92Sxk7H3Lkvyo2NMF0AlqNvPyslr0tS8d0exM4aAoqyEALw_wcB&gclsrc=aw.ds");
    $tconsumerInfoData["21"] = new PLConsumerInfoItem(8, "Very", "�12.99", "Shop Very for women's, men's and kids fashion plus furniture, homewares and electricals.", "https://www.very.co.uk/playstation-4-playstation-hits-rayman-legends-ps4/1600299214.prd?utm_campaign=other&awc=3090_1588692078_345439eb18b98831afdc65103a27f552&aff=awin&affsrc=74988&cm_mmc=awin-_-74988-_-Comparison+Engine-_-0_22454885291&utm_source=awin&utm_medium=affiliate&utm_term=Kelkoo_74988&utm_content=na");
    $tconsumerInfoData["22"] = new PLConsumerInfoItem(8, "Studio", "�12.99", "Incredible discounts on top brand fashion, home, garden, electricals and more, with FREE personalisation on 100s of gifts.", "https://www.studio.co.uk/shop/en/studio/ps4-rayman-legends?source=TK3K&utm_source=google&utm_medium=cpc&utm_campaign=--always_on--PLA%20%7C%20Entertainment%20&%20Books%20%7C%20TK3K&utm_term=--P_23405473--&utm_content=--pla--&gclid=Cj0KCQjwncT1BRDhARIsAOQF9LlG6LFhAzDKQgWnB3duKk24wU8gG1Q3nuLD3F9XnkEDY4SyqQ6LIDsaAqVwEALw_wcB");
    $tconsumerInfoData["29"] = new PLConsumerInfoItem(11, "Playstation store", "�5.79", "The PlayStation Store is a digital media store available to users of Sony's PlayStation 3, PlayStation 4, PlayStation Vita game consoles via the PlayStation Network.", "https://store.playstation.com/en-gb/product/EP4395-CUSA07208_00-HFFGAMEANDTHEME0");
    $tconsumerInfoData["32"] = new PLConsumerInfoItem(12, "GAME", "�19.99", "Game is the UK's leading games retailer with great deals on video games, consoles, accessories and the latest pre-order games.", "https://www.game.co.uk/en/gang-beasts-2633447?gclid=Cj0KCQjwncT1BRDhARIsAOQF9Ln-qMGhGUrUVGSENgcOtmm2VV64zcQATzTiwP2M5OnMT5aRPUOaG7kaAnwKEALw_wcB");
    $tconsumerInfoData["33"] = new PLConsumerInfoItem(12, "Argos", "�19.99", "Argos is an English catalogue retailer operating in the United Kingdom and Ireland, and a subsidiary of Sainsbury's.", "https://www.argos.co.uk/product/1406153?istCompanyId=a74d8886-5df9-4baa-b776-166b3bf9111c&istFeedId=c290d9a9-b5d6-423c-841d-2a559621874c&istItemId=iilipxpqi&istBid=t&&cmpid=GS001&_\$ja=tsid:59157%7cacid:534-693-8244%7ccid:9548734227%7cagid:97250270545%7ctid:pla-884259322536%7ccrid:422732075249%7cnw:g%7crnd:15975324069955131182%7cdvc:c%7cadp:%7cmt:%7cloc:1007213&utm_source=Google&utm_medium=cpc&utm_campaign=9548734227&utm_term=&utm_content=shopping&utm_custom1=97250270545&utm_custom2=534-693-8244&gclid=Cj0KCQjwncT1BRDhARIsAOQF9Lk2dqHvzIb0ODQZ3rNLDJtmY-xmqA9mGZBR-GIi3onGUGq3cLsvLiEaAtsGEALw_wcB&gclsrc=aw.ds");
    $tconsumerInfoData["34"] = new PLConsumerInfoItem(12, "Studio", "�19.99", "Incredible discounts on top brand fashion, home, garden, electricals and more, with FREE personalisation on 100s of gifts.", "https://www.studio.co.uk/shop/en/studio/gang-beasts-p-24602981--1?source=TK3K&utm_source=google&utm_medium=cpc&utm_campaign=--always_on--PLA%20%7C%20Entertainment%20&%20Books%20%7C%20TK3K&utm_term=--P_24602981--&utm_content=--pla--&gclid=Cj0KCQjwncT1BRDhARIsAOQF9Ll9axr_ty_qhFkR4PMau7TEG7RwGTetZwPfFM6rnkdDXUdga6IsL-MaAr5UEALw_wcB");
    $tconsumerInfoData["35"] = new PLConsumerInfoItem(13, "GAME", "�19.99", "Game is the UK's leading games retailer with great deals on video games, consoles, accessories and the latest pre-order games.", "https://www.game.co.uk/en/gang-beasts-2633447?gclid=Cj0KCQjwncT1BRDhARIsAOQF9Ln-qMGhGUrUVGSENgcOtmm2VV64zcQATzTiwP2M5OnMT5aRPUOaG7kaAnwKEALw_wcB");
    $tconsumerInfoData["36"] = new PLConsumerInfoItem(13, "Argos", "�19.99", "Argos is an English catalogue retailer operating in the United Kingdom and Ireland, and a subsidiary of Sainsbury's.", "https://www.argos.co.uk/product/1406153?istCompanyId=a74d8886-5df9-4baa-b776-166b3bf9111c&istFeedId=c290d9a9-b5d6-423c-841d-2a559621874c&istItemId=iilipxpqi&istBid=t&&cmpid=GS001&_\$ja=tsid:59157%7cacid:534-693-8244%7ccid:9548734227%7cagid:97250270545%7ctid:pla-884259322536%7ccrid:422732075249%7cnw:g%7crnd:15975324069955131182%7cdvc:c%7cadp:%7cmt:%7cloc:1007213&utm_source=Google&utm_medium=cpc&utm_campaign=9548734227&utm_term=&utm_content=shopping&utm_custom1=97250270545&utm_custom2=534-693-8244&gclid=Cj0KCQjwncT1BRDhARIsAOQF9Lk2dqHvzIb0ODQZ3rNLDJtmY-xmqA9mGZBR-GIi3onGUGq3cLsvLiEaAtsGEALw_wcB&gclsrc=aw.ds");
    $tconsumerInfoData["37"] = new PLConsumerInfoItem(13, "Studio", "�19.99", "Incredible discounts on top brand fashion, home, garden, electricals and more, with FREE personalisation on 100s of gifts.", "https://www.studio.co.uk/shop/en/studio/gang-beasts-p-24602981--1?source=TK3K&utm_source=google&utm_medium=cpc&utm_campaign=--always_on--PLA%20%7C%20Entertainment%20&%20Books%20%7C%20TK3K&utm_term=--P_24602981--&utm_content=--pla--&gclid=Cj0KCQjwncT1BRDhARIsAOQF9Ll9axr_ty_qhFkR4PMau7TEG7RwGTetZwPfFM6rnkdDXUdga6IsL-MaAr5UEALw_wcB");
    $tconsumerInfoData["41"] = new PLConsumerInfoItem(15, "Base", "�12.85", "Video Games, PC, Music CDs, Blu-ray, DVD, Storage, Flash Memory and more. All at competitive prices. Free UK Delivery on all purchases.", "https://www.base.com/buy/product/lego-worlds-ps4/dgc-legowrldps4.htm?gclid=Cj0KCQjwncT1BRDhARIsAOQF9Lk6j3EJgUyi35u2eD_I9aHrp-ON9PjtsmDWN2fHO-DHP2rRmBdiTP8aAprJEALw_wcB v");
    $tconsumerInfoData["42"] = new PLConsumerInfoItem(15, "Currys", "�14.99", "Massive Savings on TVs, Washing Machines, Cookers, Laptops, Headphones, Cameras, Tablets & more. We're Open Online and Delivering!", "https://www.currys.co.uk/gbuk/tv-and-home-entertainment/gaming/console-games/ps4-lego-worlds-10199552-pdt.html?istCompanyId=bec25c7e-cbcd-460d-81d5-a25372d2e3d7&istFeedId=2f489429-2329-4fcd-941a-a6a497a10e1d&istItemId=iwqlwlawa&istBid=t&awc=1599_1588702565_530a1979b4f3d726d1c234bf6a4d2d18&srcid=369&xtor=AL-1&cmpid=aff~Kelkoo~Comparison+Engine~74988~Kelkoo+Group");
    $tconsumerInfoData["43"] = new PLConsumerInfoItem(15, "GAME", "�17.99", "Game is the UK's leading games retailer with great deals on video games, consoles, accessories and the latest pre-order games.", "https://www.game.co.uk/en/lego-worlds-1770068");

    $tconsumerInfo = new PLConsumerInfoList();
    $tconsumerInfo->consumeritems = $tconsumerInfoData;
    return $tconsumerInfo;

}

//---------JSON HELPER FUNCTIONS-------------------------------------------------------
function jsonAll($pfile)
{
    $tentries = file($pfile);
    $tarray = [];
    foreach($tentries as $tentry)
    {
        $tarray[] = json_decode($tentry);
    }
    return $tarray;
}

function jsonAllencode($pfile)
{
    $tentries = file($pfile);
    $tarray = [];
    foreach($tentries as $tentry)
    {
        $tarray[] = json_encode($tentry);
    }
    return $tarray;
}
function jsonNextID($pfile)
{
    $tsplfile = new SplFileObject($pfile);
    $tsplfile->seek(PHP_INT_MAX);
    return $tsplfile->key() + 1;
}
//---------ID GENERATION FUNCTIONS-------------------------------------------------------
function jsonNextReviewID()
{
    return jsonNextID("data/json/review.json");
}
function jsonNextLoginID()
{
    return jsonNextID("data/json/login.json");
}
//---------JSON-DRIVEN OBJECT CREATION FUNCTIONS-----------------------------------------
function jsonLoadOneReview($pid) : PLReview
{
    $treview = new PLReview();
    $treview->fromArray(jsonOne("data/json/review.json",$pid));
    return $treview;
}


//--------------MANY OBJECT IMPLEMENTATION--------------------------------------------------------
function jsonLoadAllReviews() : array
{
    $tarray = jsonAll("data/json/review.json");
    return array_map(function($a){ $tc = new PLReview(); $tc->fromArray($a); return $tc; },$tarray);
}
function jsonLoadAllFeatures() : array
{
    $tarray = jsonAll("data/json/feature.json");
    return array_map(function($a){ $tc = new PLFeature(); $tc->fromArray($a); return $tc; },$tarray);
}
function jsonLoadAllOffReviews() : array
{
    $tarray = jsonAll("data/json/offreview.json");
    return array_map(function($a){ $tc = new PLOfficialReview(); $tc->fromArray($a); return $tc; },$tarray);
}
function jsonLoadAllKeyFacts() : array
{
    $tarray = jsonAll("data/json/keyfacts.json");
    return array_map(function($a){ $tc = new PLKeyFacts(); $tc->fromArray($a); return $tc; },$tarray);
}
function jsonLoadAllLogins() : array
{
    $tarray = jsonAll("data/json/login.json");
    return array_map(function($a){ $tc = new PLLogin(); $tc->fromArray($a); return $tc; },$tarray);
}


?>