<?php

function processFormData($pdata)
{
    $tclean = $pdata ?? "";
    if (! empty($tclean)) {
        $tclean = trim($tclean);
        $tclean = stripslashes($tclean);
        $tclean = htmlspecialchars($tclean);
    }
    return $tclean;
}

function nullAsEmpty(array &$pdata, $tkey)
{
    $pdata[$tkey] = $pdata[$tkey] ?? "";
}

function processRequest($pdata)
{
    $tdata = trim($pdata);
    $tdata = stripslashes($tdata);
    $tdata = htmlspecialchars($tdata);
    return $tdata;
}
/////////////////////////////////
// Function to get the Form
// Method.
/////////////////////////////////
function appFormMethod($pmethoddefault = true)
{
    return $pmethoddefault ? "POST" : "GET";
}
function appFormMethodIsPost()
{
    return strtolower($_SERVER['REQUEST_METHOD']) == 'post';
}
/////////////////////////////////
// Function to get the Form
// Action to Self-Submit.
/////////////////////////////////
function appFormActionSelf()
{
    return htmlspecialchars($_SERVER['PHP_SELF']);
}
////////////////////////////////
// Redirect To Error Page
////////////////////////////////
function appGoToError()
{
    header("Location: app_error.php");
}

////////////////////////////////
// Redirect To Error Page With Message
////////////////////////////////
function appGoToErrorMsg($pmsg)
{
    $pmsg = processFormData($pmsg);
    header("Location: app_error.php?msg={$pmsg}");
}

?>