<?php

// ----INCLUDE APIS------------------------------------
include ("api/api.inc.php");
// ----PAGE GENERATION LOGIC---------------------------
function createPage($pgamelist, $poffReviews, $precommendation, $pkeyfact, $pconsumerInfo, $previewData)
{
    $tgameProfile = "";
    $toffReviews = "";
    $trecommendation = "";
    $tkeyfact = "";
    $tconsumerInfo = "";
    $treviewData = "";

    //Populating variable with renders for page
    foreach ($pgamelist as $tg) {
        $tgameProfile .= $tg->getConsoleHTML();
    }
    foreach ($poffReviews as $tg) {
        $toffReviews .= getOffReviewHTML($tg);
    }
    foreach ($precommendation as $tg){
        $trecommendation .= $tg->getRecommendationHTML();
    }
    foreach ($pkeyfact as $tg){
        $tkeyfact .= getKeyFactsHTML($tg);
    }
    $counter = 0;
    foreach ($pconsumerInfo as $tg){
        $tconsumerInfo .= $tg->getConsumerInfoHTML();
        $counter += 1;
    }
    if ($counter < 3) {
        $tconsumerInfo .= <<<MESSAGE
            <p>There are no other retailers who sell this product.
MESSAGE;
    }
    if(!empty($previewData)){
        foreach ($previewData as $tr){

            $treviewData .= getReviewHTML($tr);

        }
    }else{
        $treviewData = <<<EMPTY
                <p>There are no reviews for this game.
EMPTY;
    }
    $tuserReview = <<<REV
            <button class="col-xs-3 pull-right"><a href="review_entry.php">Create a reivew</a></button>
REV;
    $tuserCreateReview = <<<LOG
            <p class="text-primary col-xs-3 pull-right">Login to create a review
LOG;
    $auth = isset($_SESSION["logged_in"]) ? $tuserReview : $tuserCreateReview; //If a user is logged in create a review will show

    $tcontent = <<<PAGE
        {$tgameProfile}
        {$trecommendation}
        {$toffReviews}
        {$tkeyfact}
        <article class="col-xs-6">
            <h4>Consumer Information:</h4>
            {$tconsumerInfo}
        </article>
        {$auth}
        <h5>Reviews</h5>
        {$treviewData}
        PAGE;
    return $tcontent;
}

// ----BUSINESS LOGIC---------------------------------
header('Content-Type: text/html; charset=ISO-8859-1'); //Character set to display � symbol
// Use our Data Access Layer to create our list of games
$tgamelist = DAL_CreateGames("");
$toffReviews = jsonLoadAllOffReviews();
$trecommendations = DAL_CreateRecommendations("sort");
$tkeyFacts = jsonLoadAllKeyFacts();
$tconsumerInfoList = DAL_CreateConsumerInfo();
$treviews = jsonLoadAllReviews();

$tgames = [];
$toffReview = [];
$trecommendation = [];
$tkeyFact = [];
$tconsumerInfo = [];
$treviewData = [];


$tid = $_REQUEST["id"] ?? - 1;
$ttitle = $_REQUEST["title"] ?? "";

// Handle our Requests and Search for Games
if (is_numeric($tid) && $tid > 0) {
    foreach ($tgamelist->gameitems as $tg) {
        if ($tid == $tg->id) {
            $tgames[] = $tg;
            break;
        }
    }
} else if (! empty($ttitle)) {
    if (count($tgames) <= 0) {
        appGoToError();
        return;
    }
}
//Collecting the correct data for the specific game
foreach ($toffReviews as $toffr) {
    if ($tid == $toffr->gameId) {
        $toffReview[] = $toffr;
        break;
    }
}
foreach ($trecommendations->recommendationitems as $trecom) {
    if ($tid == $trecom->gameId) {
        $trecommendation[] = $trecom;
        break;
    }
}
foreach ($tkeyFacts as $tkf) {
    if ($tid == $tkf->gameId) {
        $tkeyFact[] = $tkf;
        break;
    }
}
foreach ($tconsumerInfoList->consumeritems as $tci) {
    if ($tid == $tci->gameId) {
        $tconsumerInfo[] = $tci;
    }
}
foreach ($treviews as $tr) {
    if ($tid == $tr->gameId) {
        $treviewData[] = $tr;
    }
}
// We've found our Game
$tpagecontent = createPage($tgames, $toffReview, $trecommendation, $tkeyFact, $tconsumerInfo, $treviewData);
$tpagetitle = "Game Details";
$tpagelead = "";
$tpagefooter = "";

// ----BUILD OUR HTML PAGE----------------------------
// Create an instance of our Page class
$tpage = new MasterPage($tpagetitle);
// Set the Three Dynamic Areas (1 and 3 have defaults)
if (! empty($tpagelead))
    $tpage->setDynamic1($tpagelead);
$tpage->setDynamic2($tpagecontent);
if (! empty($tpagefooter))
    $tpage->setDynamic3($tpagefooter);
// Return the Dynamic Page to the user.
$tpage->renderPage();
?>